# Hello World

- Point 1
- Point 2

---
## slides Primary

This is the first slide

----

Nice vertical slide

----

Another vertical slide

---

## Code example with

#### Highlighting

```html [1|2-3|4|5]
<div class="reveal">
    <div class="slides">
        <section data-markdown="slides.md"></section>
    </div>
</div>
```

Some text `with` highlighting

<!-- .slide: data-background="#fbf000" -->
## Continue to see fade-in items by pressing "<" or ">"
<!-- .element: class="fragment" data-fragment-index="1" -->
<!-- .element: class="fragment" data-fragment-index="2" -->
<!-- .element: class="fragment" data-fragment-index="3" -->
<!-- .element: class="fragment" data-fragment-index="4" -->

---
## An image

![USMC](./images/USMC.png)

----

## And the final slide
